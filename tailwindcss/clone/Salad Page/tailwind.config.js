const colors = require('tailwindcss/colors')

module.exports = {
  theme: {
    extend: {
      colors: {
        'light-blue': colors.lightBlue,
        cyan: colors.cyan,
        brown: {
          100: '#c98f66',
        },
      },
      animation: {
        'spin-slow': 'spin 3s linear infinite',
        'bounce-slow': 'bounce 1.5s ease-in-out infinite',
      },
    },
  },
  variants: {
    ringColor: ['responsive', 'hover', 'dark', 'focus-within', 'focus'],
    ringOffsetColor: ['responsive', 'hover', 'dark', 'focus-within', 'focus'],
    ringOffsetWidth: ['responsive', 'hover', 'focus-within', 'focus'],
    ringOpacity: ['responsive', 'hover', 'focus-within', 'focus'],
    ringWidth: ['responsive', 'hover', 'focus-within', 'focus'],
    animation: ['hover', 'focus', 'group-hover'],
    transform: ['hover', 'focus', 'group-hover'],
  },
  plugins: [],
}
